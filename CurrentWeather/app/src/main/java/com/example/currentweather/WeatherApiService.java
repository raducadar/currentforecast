package com.example.currentweather;


import com.example.currentweather.data.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApiService {
     String API_KEY = "e67a8ba5d1c788c463f99ce9e50d979b";
     //http://api.weatherstack.com/current?access_key=e67a8ba5d1c788c463f99ce9e50d979b

     @GET("current")
     Call<WeatherResponse> getCurrentWeather(@Query("access_key") String API_KEY, @Query("query") String cityName);

}
