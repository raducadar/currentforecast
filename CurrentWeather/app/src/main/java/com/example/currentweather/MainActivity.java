package com.example.currentweather;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.currentweather.data.WeatherResponse;
import com.example.currentweather.fragments.SettingsFragment;
import com.example.currentweather.fragments.TodayFragment;
import com.example.currentweather.fragments.WeekFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setFragment(new TodayFragment());

       /* final TextView todayTextView = findViewById(R.id.today_textView);




        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.weatherstack.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WeatherApiService weatherApiService = retrofit.create(WeatherApiService.class);
        String API_KEY = "e67a8ba5d1c788c463f99ce9e50d979b";
        Call<WeatherResponse> response = weatherApiService.getCurrentWeather(API_KEY, "Tokyo");

        response.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if(!response.isSuccessful()){
                    todayTextView.setText("Code: " + response.code());
                    return;
                }
                WeatherResponse weatherResponse = response.body();
                todayTextView.setText(weatherResponse.getCurrent().getTemperature());
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


*/

        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(navListener);
        }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener()
            {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.id_today_weather_item:
                            selectedFragment = new TodayFragment();
                            break;
                        case R.id.id_week_weather_item:
                            selectedFragment = new WeekFragment();
                            break;
                        case R.id.id_settings_weather_item:
                            selectedFragment = new SettingsFragment();
                            break;
                    }

                    if(selectedFragment != null)
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();
                    return true;
                }
            };



}